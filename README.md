# Internship Repository Manager
### By Michiel Mulder - Student at AP Hogeschool

#### Teachers

* Tim Dams - <tim.dams@ap.be>
* Tom Peeters - <tom.peeters@ap.be>

#### Project description

A website to help teachers manage the repositories of the internships and their corresponding projects and theses. Through this tool, teachers will be able to find basic information of the students and their projects. Furthermore, they can see a useful overview of the activities and stats concerning student's repositories.